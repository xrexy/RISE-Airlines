﻿namespace Airlines.Persistence.Basic.Entities;

public partial class Airline
{
    public int Id { get; set; }

    public string Code { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    public int FleetSize { get; set; }

    public int Founded { get; set; }

    public virtual ICollection<Flight> Flights { get; set; } = new List<Flight>();
}
