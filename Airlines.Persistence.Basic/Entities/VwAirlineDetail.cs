﻿namespace Airlines.Persistence.Basic.Entities;

public partial class VwAirlineDetail
{
    public int AirlineId { get; set; }

    public string AirlineCode { get; set; } = null!;

    public string Airline { get; set; } = null!;

    public string AirlineDescription { get; set; } = null!;

    public int AirlineFleetSize { get; set; }

    public int AirlineFounded { get; set; }
}
