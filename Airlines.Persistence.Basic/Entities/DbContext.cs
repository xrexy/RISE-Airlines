﻿using Airlines.Persistence.Basic.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace Airlines.Persistence.Basic.Entities;

// generated using `dotnet ef dbcontext scaffold '{connection string} Microsoft.EntityFrameworkCore.SqlServer -o Entities`
public partial class DbContext : Microsoft.EntityFrameworkCore.DbContext
{
    public enum ConnectionType
    {
        SqlServer = 0,
        InMemory = 1,
    }

    private bool IsInMemory { get; }
    private string? InMemoryDatabaseName { get; }

#pragma warning disable CS8618
    public DbContext()
    {
    }

    public DbContext(DbContextOptions<DbContext> options)
        : base(options)
    {
    }

    public DbContext(ConnectionType type, string? inMemoryDatabaseName = null)
    {
        IsInMemory = type == ConnectionType.InMemory;
        InMemoryDatabaseName = inMemoryDatabaseName;
    }
#pragma warning restore

    public virtual DbSet<Airline> Airlines { get; set; }

    public virtual DbSet<Airport> Airports { get; set; }

    public virtual DbSet<Flight> Flights { get; set; }

    public virtual DbSet<VwAirlineDetail> VwAirlineDetails { get; set; }

    public virtual DbSet<VwAirlineView> VwAirlineViews { get; set; }

    public virtual DbSet<VwAirportDetail> VwAirportDetails { get; set; }

    public virtual DbSet<VwFlightDetail> VwFlightDetails { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (optionsBuilder.IsConfigured) return;

        var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        // var isDev = env.IsNullOrEmpty() || !string.Equals(env, "PRODUCTION", StringComparison.OrdinalIgnoreCase);
        // if (isDev)
        // {
        //     _ = optionsBuilder
        //         .EnableSensitiveDataLogging()
        //         .LogTo(Console.WriteLine, Microsoft.Extensions.Logging.LogLevel.Debug);
        // }

        if (IsInMemory || string.Equals(env, "testing", StringComparison.OrdinalIgnoreCase))
        {
            _ = optionsBuilder.UseInMemoryDatabase(InMemoryDatabaseName ?? "InMemoryDb");
        }
        else
        {
            var connString = ConfigurationManager.GetConnectionString("DefaultConnection");
            if (connString.IsNullOrEmpty()) throw new Exception("Connection string is null");

            _ = optionsBuilder.UseSqlServer(connString);
        }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        _ = modelBuilder.Entity<Airline>(entity =>
        {
            _ = entity.HasKey(e => e.Id).HasName("PK__Airlines__3214EC07B8126DE2");

            _ = entity.HasIndex(e => e.Code, "UQ__Airlines__A25C5AA79F73BC00").IsUnique();

            _ = entity.Property(e => e.Code).HasMaxLength(2);
            _ = entity.Property(e => e.Description).HasMaxLength(256);
            _ = entity.Property(e => e.Name).HasMaxLength(128);
        })
            .Entity<Airport>(entity =>
        {
            _ = entity.HasKey(e => e.Id).HasName("PK__Airports__3214EC072642193D");

            _ = entity.HasIndex(e => e.Code, "UQ__Airports__A25C5AA79F298066").IsUnique();

            _ = entity.Property(e => e.City).HasMaxLength(128);
            _ = entity.Property(e => e.Code).HasMaxLength(3);
            _ = entity.Property(e => e.Country).HasMaxLength(128);
            _ = entity.Property(e => e.Name).HasMaxLength(128);
        })
            .Entity<Flight>(entity =>
        {
            _ = entity.HasKey(e => e.Id).HasName("PK__Flights__3214EC079217FC79");

            _ = entity.HasIndex(e => e.ArrivalDateTime, "IDX_FlightsArrival");

            _ = entity.HasIndex(e => e.DepartureDateTime, "IDX_FlightsDeparture");

            _ = entity.HasIndex(e => e.FromAirportId, "IDX_FlightsFromAirport");

            _ = entity.HasIndex(e => e.ToAirportId, "IDX_FlightsToAirport");

            _ = entity.HasIndex(e => e.FlightNumber, "UQ__Flights__2EAE6F50F847F63F").IsUnique();

            _ = entity.Property(e => e.ArrivalDateTime).HasColumnType("datetime");
            _ = entity.Property(e => e.DepartureDateTime).HasColumnType("datetime");
            _ = entity.Property(e => e.FlightNumber).HasMaxLength(10);

            _ = entity.HasOne(d => d.Airline).WithMany(p => p.Flights)
                .HasForeignKey(d => d.AirlineId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Airline");

            _ = entity.HasOne(d => d.FromAirport).WithMany(p => p.FlightFromAirports)
                .HasForeignKey(d => d.FromAirportId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_FromAirport");

            _ = entity.HasOne(d => d.ToAirport).WithMany(p => p.FlightToAirports)
                .HasForeignKey(d => d.ToAirportId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ToAirport");
        })
            .Entity<VwAirlineDetail>(entity =>
        {
            _ = entity
                .HasNoKey()
                .ToView("vw_AirlineDetails");

            _ = entity.Property(e => e.Airline).HasMaxLength(128);
            _ = entity.Property(e => e.AirlineCode).HasMaxLength(2);
            _ = entity.Property(e => e.AirlineDescription).HasMaxLength(256);
            _ = entity.Property(e => e.AirlineId).ValueGeneratedOnAdd();
        })
            .Entity<VwAirlineView>(entity =>
        {
            _ = entity
                .HasNoKey()
                .ToView("vw_AirlineView");

            _ = entity.Property(e => e.Airline).HasMaxLength(128);
            _ = entity.Property(e => e.AirlineCode).HasMaxLength(2);
            _ = entity.Property(e => e.AirlineDescription).HasMaxLength(256);
            _ = entity.Property(e => e.AirlineId).ValueGeneratedOnAdd();
        })
            .Entity<VwAirportDetail>(entity =>
        {
            _ = entity
                .HasNoKey()
                .ToView("vw_AirportDetails");

            _ = entity.Property(e => e.Airport).HasMaxLength(128);
            _ = entity.Property(e => e.AirportCity).HasMaxLength(128);
            _ = entity.Property(e => e.AirportCode).HasMaxLength(3);
            _ = entity.Property(e => e.AirportCountry).HasMaxLength(128);
            _ = entity.Property(e => e.AirportId).ValueGeneratedOnAdd();
        })
            .Entity<VwFlightDetail>(entity =>
        {
            _ = entity
                .HasNoKey()
                .ToView("vw_FlightDetails");

            _ = entity.Property(e => e.Airline).HasMaxLength(128);
            _ = entity.Property(e => e.AirlineCode).HasMaxLength(2);
            _ = entity.Property(e => e.AirlineDescription).HasMaxLength(256);
            _ = entity.Property(e => e.Airport).HasMaxLength(128);
            _ = entity.Property(e => e.AirportCity).HasMaxLength(128);
            _ = entity.Property(e => e.AirportCode).HasMaxLength(3);
            _ = entity.Property(e => e.AirportCountry).HasMaxLength(128);
            _ = entity.Property(e => e.ArrivalDateTime).HasColumnType("datetime");
            _ = entity.Property(e => e.DepartureDateTime).HasColumnType("datetime");
            _ = entity.Property(e => e.FlightNumber).HasMaxLength(10);
        });

        this.OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
