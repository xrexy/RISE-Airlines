-- Create a new view called 'vw_FlightView'

USE NemetschekConsoleApp;
GO

DROP VIEW IF EXISTS vw_FlightDetails;
GO

CREATE VIEW vw_FlightDetails
AS
  SELECT
    fl.Id as FlightId,
    fl.FlightNumber,
    fl.DepartureDateTime,
    fl.ArrivalDateTime,
    al.*,
    fl.FromAirportId,
    fromAirport.City as FromCity,
    fromAirport.Country as FromCountry,
    fromAirport.Code as FromCode,
    fromAirport.Runways as FromRunways,
    fromAirport.Name as FromName,
    fromAirport.Founded as FromFounded,
    fl.ToAirportId,
    toAirport.City as ToCity,
    toAirport.Country as ToCountry,
    toAirport.Code as ToCode,
    toAirport.Runways as ToRunways,
    toAirport.Name as ToName,
    toAirport.Founded as ToFounded
  FROM dbo.Flights fl
    JOIN dbo.vw_AirlineDetails al on fl.AirlineId = al.AirlineId
    JOIN dbo.Airports fromAirport ON fl.FromAirportId = fromAirport.Id
    JOIN dbo.Airports toAirport ON fl.ToAirportId = toAirport.Id
GO
