-- Create query which shows all the flights scheduled for tomorrow with all possible details for Airports, Airlines and Flight

USE NemetschekConsoleApp;
GO

SELECT fl.*
FROM dbo.vw_FlightDetails fl
WHERE CONVERT(DATE, fl.DepartureDateTime) = CONVERT(DATE, GETDATE() + 1)
ORDER BY fl.FlightNumber
