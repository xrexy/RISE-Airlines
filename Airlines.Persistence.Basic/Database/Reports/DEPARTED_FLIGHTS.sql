-- Show the count of all departed flights

USE NemetschekConsoleApp;
GO

SELECT Count(fl.Id) as DepartedFlightsCount
FROM dbo.Flights fl
WHERE fl.DepartureDateTime < GETDATE();
