namespace Airlines.Persistence.Basic.Models.Baggage;

public static class SmallBaggage
{
    public static uint MaximumWeight => 15;
    public static double MaximumVolume => 0.045;
}
