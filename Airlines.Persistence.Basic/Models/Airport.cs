namespace Airlines.Persistence.Basic.Models;

public class Airport(string identifier, string name, string city, string country) : IModel
{
    public string Identifier => identifier.ToUpper();
    public string Name => name;
    
    public string City => city;
    public string Country => country;
}
