namespace Airlines.Persistence.Basic.Models;

public class Flight(
    string identifier,
    string departureAirport,
    string arrivalAirport,
    string aircraftModel,
    uint price,
    double timeInHours
) : IModel
{
    public string Identifier => identifier.ToUpper();
    public string AircraftModel => aircraftModel;
    public string DepartureAirport => departureAirport.ToUpper();
    public string ArrivalAirport => arrivalAirport.ToUpper();
    public uint Price => price;
    public double TimeInHours => timeInHours;

    public uint SeatsUsed { get; set; }
    public uint WeightUsed { get; set; }
    public double VolumeUsed { get; set; }
}
