namespace Airlines.Persistence.Basic.Models.AircraftTypes;

public class CargoAircraftType(uint weight, double volume) : ICargoAircraftType
{
    public uint CargoWeight => weight;
    public double CargoVolume => volume;
}
