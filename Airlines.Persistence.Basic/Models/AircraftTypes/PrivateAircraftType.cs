namespace Airlines.Persistence.Basic.Models.AircraftTypes;

public class PrivateAircraftType(uint seats) : ITicketableAircraftType
{
  public uint Seats => seats;
}
