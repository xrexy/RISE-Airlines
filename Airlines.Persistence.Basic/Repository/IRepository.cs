using System.Linq.Expressions;

namespace Airlines.Persistence.Basic.Repository;

public interface IRepository<T> : IDisposable where T : class
{
    /// <summary>Gets all entities that satisfy the predicate</summary>
    /// <param name="predicate">The predicate to use</param>
    /// <returns>A list of entities that satisfy the predicate</returns>
    public Task<List<T>> GetAll(Expression<Func<T, bool>> predicate);

    /// <summary>Gets an entity that satisfies the predicate</summary>
    /// <param name="predicate">The predicate to use</param>
    /// <returns>An entity that satisfies the predicate</returns>
    public Task<T?> GetSingle(Expression<Func<T, bool>> predicate);

    /// <summary>Gets all stored entities</summary>
    /// <returns>A list of all stored entities</returns>
    public Task<List<T>> GetAllAsync();

    /// <summary>Adds an entity to the database</summary>
    /// <returns>Id of the added entity, or -1 if the operation failed</returns>
    /// <exception cref="InvalidEntityException">Thrown if the entity is invalid</exception>
    public Task<int> AddAsync(T entity);

    /// <summary>Updates an entity in the database</summary>
    /// <returns>true, if the entity was updated, false otherwise</returns>
    public Task<bool> UpdateAsync(T entity);

    /// <summary>Deletes an entity from the database</summary>
    /// <param name="id">The id of the entity to delete</param>
    /// <returns>true, if the entity was successfully deleted, false otherwise</returns>
    public Task<bool> DeleteAsync(int id);

    /// <summary>Deletes an entity from the database</summary>
    /// <remarks>Equal to #DeleteAsync(entity.Id)</remarks>
    /// <param name="entity">The entity to delete</param>
    /// <returns>true, if the entity was successfully deleted, false otherwise</returns>
    public Task<bool> DeleteAsync(T entity);

    /// <summary>Checks if an entity with the specified id exists in the database</summary>
    /// <param name="id">The id of the entity</param>
    /// <returns>true, if the entity exists, false otherwise</returns>
    public Task<bool> ExistsAsync(int id);

    /// <summary>Checks if an entity with the specified id exists in the database</summary>
    /// <param name="entity">The entity</param>
    /// <returns>true, if the entity exists, false otherwise</returns>
    public Task<bool> ExistsAsync(T entity);

    /// <summary>Checks if an entity with the specified predicate exists in the database</summary>
    /// <param name="predicate">The predicate to use</param>
    /// <returns>true, if an entity exists, false otherwise</returns>
    public Task<bool> ExistsPredicateAsync(Expression<Func<T, bool>> predicate);

    /// <summary>Preprocesses an entity before adding it to the database</summary>
    /// <remarks>Does both validation and normalization</remarks>
    /// <param name="entity">The entity to preprocess</param>
    /// <exception cref="InvalidEntityException">Thrown if the entity has an invalid property</exception>
    public void PreprocessEntity(T entity);
}
