using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repository.InMemory;

[Obsolete("This is an in-memory repository. A DB based one should be used instead.")]
public class AirportRepository : IRepository<Airport>
{
    public Dictionary<string, List<Airport>> CityAirports { get; } = [];
    public Dictionary<string, List<Airport>> CountryAirports { get; } = [];

    public int Size => this.GetAll().Count();

    public int CitySize => CityAirports.Count;
    public int CountrySize => CountryAirports.Count;

    public bool Add(Airport entity)
    {
        if (this.Exists(entity))
        {
            return false;
        }

        this.Set(entity);
        return true;
    }

    public bool Delete(string id)
    {
        var key = id.ToUpper();
        var airport = this.GetById(key);
        if(airport == null) return false;

        return CityAirports[airport.City.ToUpper()].Remove(airport) && CountryAirports[airport.Country.ToUpper()].Remove(airport);
    }

    public bool Delete(Airport entity)
        => this.Delete(entity.Identifier);

    public void DeleteAll()
    {
        CityAirports.Clear();
        CountryAirports.Clear();
    }

    public bool Exists(string id)
        => CityAirports
            .SelectMany(x => x.Value)
            .Any(x => string.Equals(x.Identifier, id, StringComparison.OrdinalIgnoreCase));

    public bool Exists(Airport entity)
        => this.Exists(entity.Identifier);

    public Airport? GetById(string id)
        => CityAirports
            .SelectMany(x => x.Value)
            .FirstOrDefault(x => string.Equals(x.Identifier, id, StringComparison.OrdinalIgnoreCase));

    public IEnumerable<Airport> GetAll()
      => CityAirports
        .SelectMany(x => x.Value);

    public IEnumerable<Airport> GetAllByCountry(string country)
    {
        var values = CountryAirports.GetValueOrDefault(country.ToUpper());
        return values ?? [];
    }

    public IEnumerable<Airport> GetAllByCity(string city)
    {
        var values = CityAirports.GetValueOrDefault(city.ToUpper());
        return values ?? [];
    }

    public bool Update(Airport entity)
    {
        if (!this.Exists(entity))
        {
            return false;
        }

        this.Set(entity);
        return true;
    }

    private void Set(Airport entity)
    {

        var key = entity.City.ToUpper();
        if (CityAirports.GetValueOrDefault(key) == null) CityAirports[key] = [];
        CityAirports[key].Add(entity);

        key = entity.Country.ToUpper();
        if (CountryAirports.GetValueOrDefault(key) == null) CountryAirports[key] = [];
        CountryAirports[key].Add(entity);
    }
}
