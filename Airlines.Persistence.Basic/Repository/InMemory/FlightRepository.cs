using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repository.InMemory;

[Obsolete("This is an in-memory repository. A DB based one should be used instead.")]
public class FlightRepository(AircraftRepository aircraftRepository) : IRepository<Flight>
{
    private Dictionary<string, Flight> Flights { get; } = [];

    public int Size => Flights.Count;

    public bool Add(Flight entity)
    {
        if (this.Exists(entity))
        {
            return false;
        }

        if (!aircraftRepository.Exists(entity.AircraftModel))
        {
            return false;
        }

        this.Set(entity);
        return true;
    }

    public bool Delete(string id)
        => Flights.Remove(id.ToUpper());

    public bool Delete(Flight entity)
        => this.Delete(entity.Identifier);

    public void DeleteAll()
        => Flights.Clear();

    public bool Exists(string id)
        => Flights.ContainsKey(id.ToUpper());

    public bool Exists(Flight entity)
        => this.Exists(entity.Identifier);

    public Flight? GetById(string id)
        => Flights.GetValueOrDefault(id.ToUpper());

    public IEnumerable<Flight> GetAll()
        => Flights.Values;

    public bool Update(Flight entity)
    {
        if (!this.Exists(entity))
        {
            return false;
        }

        this.Set(entity);
        return true;
    }

    private void Set(Flight entity)
        => Flights[entity.Identifier.ToUpper()] = entity;
}
