# Rise Airlines

A simple CLI application for playing around with airports, airlines and flights. Built for Nemetschek Rise 2024.

# Usage
```bash
# build the application
dotnet build
```

```bash
# start the application (linux)
./Airlines.Console/bin/Debug/net8.0/Airlines.Console

# start the application (windows)
.\Airlines.Console\bin\Debug\net8.0\Airlines.Console.exe
```

## Start Page:
```
--- Airlines Console ---

[1] Add Entry
[2] Find Airports
[3] Routes
[4] Booking
[5] Batching
(6) Easy Access(Airline)
(7) Search

(0) Exit
```

## Action Prompt:
```
--- Airlines Console ---

[*] Must be at least 4 characters

Name: 
```

[*] has the validation hint. When an input is invalid the program will wipe it and re-prompt the same input.


## Feedback
```
--- Airlines Console ---

[*] Added Airline: american(AA)

(1) Add Airline
(2) Add Flight
(3) Add Airport

(0) Go Back
```

After completing all inputs correctly some actions will give you feedback. Always indicated with [*] and on the third line.

# Navigation
Navigation is done with 1-9 keys.

Items whose index is surrounded by [] (ex. [2]) have children, and items that are surrounded by () (ex. (2)) are actions.

# Development
## MSSQL Setup
After setting up an MSSQL instance navigate to  `./Airines.Web` and copy `appsettings.json` to `appsettings.Development.json` and input your connection string in `ConnectionStrings.DefaultConnections`.

## Setup Database(locally)
All the setup scripts are located in `./Airlines.Persistence.Basic/Database/Setup`, and are sorted by the order they should be executed.
```bash
# Run all setup scripts
# NOTE: If you're getting a certificate error you can add the -C flag to trust server certificate

# Automated (linux)
for file in ./Airlines.Persistence.Basic/Database/Setup/*.sql; do echo "sqlcmd -i $file"; sqlcmd -S $HOST -U $USER -P '$PASSWORD' -i $file; done;

# Manual
sqlcmd -S $HOST -U $USER -P '$PASSWORD' -i ./Airlines.Persistence.Basic/Database/Setup/$ID.$NAME.sql -C
```

*You can also run the scripts in `Reports` and TEST_CRUD_OPERATIONS.sql to make sure everything is working correctly.*

## Re-generating entities
Since this project uses the database-first approach you must first make the changes in your database(use a development one please). 

* Install dotnet-ef tool
```bash
dotnet tool install --global dotnet-ef
```

* Applying changes (overwriting our Entities)
```bash
# cd ./Airlines.Persistence.Basic/
# dotnet restore
dotnet ef dbcontext scaffold '{connection string} Microsoft.EntityFrameworkCore.SqlServer -o Entities
```

## Testing
Testing is automatically handled when you push to the repository, but you should always make sure it works locally before wasting valuable CI time :)

### Basic
```bash
ASPNETCORE_ENVIRONMENT=Testing dotnet test -l "console;verbosity=detailed"
```

### Specific Tests
```bash

# Run only tests prefixed with "ConsoleMenu_"
ASPNETCORE_ENVIRONMENT=Testing dotnet test --filter ConsoleMenu_ -l "console;verbosity=detailed"
```

### With Coverage
```bash
ASPNETCORE_ENVIRONMENT=Testing dotnet test --verbosity normal --logger "trx;LogFileName=test_results.xml" /p:CollectCoverage=true /p:CoverletOutputFormat=cobertura /p:CoverletOutput=./TestResults/ /p:Threshold=50 /p:ThresholdType=line /p:ThresholdStat=total /p:ExcludeByFile="**/Entities/*.cs"
```

# Deployment
Build the application
```bash
docker compose up -d --build
```

After the mssql service starts successfully bash into it and run the setup scripts
```bash
$ docker exec mssql bash
# or
$ docker exec -it mssql bash

# run setup
mssql@ID:/$ /setup-mssql.sh

# Test using the TEST_CRUD_OPERATIONS.sql script(also adds test information)
mssql@ID:/$ /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "$SA_PASSWORD" -i "/mssql-scripts/TEST_CRUD_OPERATIONS.sql"
```

Now the application is available at `http://localhost:8080`
