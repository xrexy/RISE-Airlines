
using Airlines.Business.DTO;
using Airlines.Business.Mapper;
using Airlines.Persistence.Basic.Repository;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("/api/airline")]
public class AirlineApiController(IAirlineRepository repository, IAirlineMapper mapper) : ControllerBase
{
    [HttpGet("all")]
    public IActionResult All()
      => this.Ok(repository.GetAllAsync().Result.ConvertAll(mapper.Map));

    [HttpGet("code/{code}")]
    public IActionResult GetByCode(string code)
    {
        var airline = repository.GetSingle(x => x.Code.ToLower().Equals(code.ToLower())).Result;
        if (airline == null) return this.BadRequest();

        return this.Ok(mapper.Map(airline));
    }

    [HttpGet("{id}")]
    public IActionResult GetById(int id)
    {
        var airline = repository.GetSingle(x => x.Id == id).Result;
        if (airline == null) return this.BadRequest();

        return this.Ok(mapper.Map(airline));
    }

    [HttpPost("add")]
    public IActionResult Add([FromBody] AirlineDTO airline)
    {
        if (!ModelState.IsValid) return this.BadRequest(ModelState);

        var entity = mapper.Map(airline);
        var res = repository.AddAsync(entity).Result;

        if(res == -1) return this.StatusCode(500);
        return this.Ok(new { Id = res }); // returns the id of the new airline
    }

    [HttpPut("update")]
    public IActionResult Update([FromBody] AirlineDTO airline)
    {
        if (!ModelState.IsValid) return this.BadRequest(ModelState);

        var entity = mapper.Map(airline);
        var isSuccess = repository.UpdateAsync(entity).Result;

        if (!isSuccess) return this.StatusCode(500);
        return this.Ok();
    }

    [HttpDelete("delete/{id}")]
    public IActionResult Delete(int id)
    {
        var isSuccess = repository.DeleteAsync(id).Result;
        if (!isSuccess) return this.StatusCode(500);
        return this.Ok();
    }
}
