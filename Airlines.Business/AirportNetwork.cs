using Airlines.Persistence.Basic.Models;
using System.Data;

namespace Airlines.Business.Structures;

public enum NetworkMatrixType
{
    Cheapest = 0,
    FewestStops = 1,
}

public class AirportNetwork
{
    // edge = flight
    public class Edge
    {
        public uint Price;
        public double TimeInHours;
        public Node? Parent;
        public Node? Child;
    }
    public class Node
    {
        public Airport Airport;
        public Node current_node;
        public List<Edge> Edges = [];

        public Node(Airport airport)
        {
            Airport = airport;
            current_node = this;
        }
        public void AddEdge(Node child, uint price, double timeInHours)
        {
            Edges.Add(new Edge()
            {
                Parent = current_node,
                Child = child,
                Price = price,
                TimeInHours = timeInHours
            });

            if (!child.Edges.Exists(a => a.Parent == child && a.Child == current_node))
            {
                child.AddEdge(current_node, price, timeInHours);
            }
        }
    }

    public List<Node> Nodes = [];
    public string[] Labels => Nodes.Select(n => n.Airport.Identifier).ToArray();

    public Node CreateNode(Airport airport)
    {
        var n = new Node(airport);
        Nodes.Add(n);
        return n;
    }

    public double?[,] CreateAdjMatrix(NetworkMatrixType type)
    {
        var adj = new double?[Nodes.Count, Nodes.Count];
        for (var i = 0; i < Nodes.Count; i++)
        {
            var node1 = Nodes[i];
            for (var j = 0; j < Nodes.Count; j++)
            {
                var node2 = Nodes[j];
                var edge = node1.Edges.Find(a => a.Child == node2);
                var weight = type switch
                {
                    NetworkMatrixType.Cheapest => edge?.Price ?? 0,
                    NetworkMatrixType.FewestStops => edge?.TimeInHours ?? 0,
                    _ => throw new InvalidOperationException("Invalid network matrix type")
                };
                adj[i, j] = weight;
            }
        }
        return adj;
    }

    public Node? GetNode(string identifier)
        => Nodes.Find(node => node.Airport.Identifier.Equals(identifier, StringComparison.CurrentCultureIgnoreCase));

    public bool AreConnected(Node startNode, Node destNode)
    {
        var visited = new HashSet<Node>();
        return this.DFS(startNode, destNode, visited);
    }

    private bool DFS(Node current, Node destination, HashSet<Node> visited)
    {
        // If the current node is the destination, return true
        if (current == destination)
        {
            return true;
        }

        // Mark the current node as visited
        _ = visited.Add(current);

        // Recur for all the nodes adjacent to the current node
        foreach (var edge in current.Edges)
        {
            var nextNode = edge.Child;
            if (nextNode == null) continue;

            // If nextNode is not visited, recur for nextNode
            if (!visited.Contains(nextNode) && this.DFS(nextNode, destination, visited))
            {
                return true;
            }
        }

        // If DFS doesn't find the destination node from the current node's connections
        return false;
    }
}
