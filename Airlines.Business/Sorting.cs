namespace Airlines.Business.Sorting;

public enum SortingAlgorithm
{
    None = 0,
    BubbleSort = 1,
    SelectionSort = 2,
}

// descending or ascending
public enum SortOrder
{
    Ascending = 0,
    Descending = 1
}

public static class Sorting
{
    public static T[] BubbleSort<T>(T[] array, SortOrder order = SortOrder.Ascending) where T : IComparable
    {
        for (var i = 0; i < array.Length - 1; i++)
        {
            var swapped = false;
            for (var j = 0; j < array.Length - i - 1; j++)
            {
                var comparison = array[j].CompareTo(array[j + 1]);
                if ((order == SortOrder.Ascending && comparison > 0) || (order == SortOrder.Descending && comparison < 0))
                {
                    // Swap arr[j+1] and arr[j]
                    (array[j + 1], array[j]) = (array[j], array[j + 1]);
                    swapped = true;
                }
            }

            if (!swapped)
            {
                break;
            }
        }
        return array;
    }


    public static T[] SelectionSort<T>(T[] array, SortOrder order = SortOrder.Ascending) where T : IComparable
    {
        for (var i = 0; i < array.Length - 1; i++)
        {
            var minIndex = i;
            for (var j = i + 1; j < array.Length; j++)
            {
                var comparison = array[j].CompareTo(array[minIndex]);
                if ((order == SortOrder.Ascending && comparison < 0) || (order == SortOrder.Descending && comparison > 0))
                {
                    minIndex = j;
                }
            }

            if (minIndex == i)
            {
                continue;
            }

            // Swap the found minimum element with the first element
            (array[minIndex], array[i]) = (array[i], array[minIndex]);
        }
        return array;
    }


    public static T[] Sort<T>(T[] arr, SortingAlgorithm algorithm, SortOrder order = SortOrder.Ascending) where T : IComparable
    {
        return algorithm switch
        {
            SortingAlgorithm.BubbleSort => BubbleSort(arr, order),
            SortingAlgorithm.SelectionSort => SelectionSort(arr, order),
            SortingAlgorithm.None => arr,
            _ => throw new InvalidAlgorithmException("Invalid sorting algorithm")
        };
    }
}
