public static class FileReader
{
    public static List<string> ReadLines(string path)
    {
        var lines = new List<string>();
        using (var reader = new StreamReader(path))
        {
            string? line;
            while ((line = reader.ReadLine()) != null)
            {
                lines.Add(line);
            }
        }

        return lines;
    }
}
