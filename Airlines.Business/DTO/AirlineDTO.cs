using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.DTO;

public class AirlineDTO
{
    [Required]
    [Display(Name = "Code")]
    [StringLength(2, ErrorMessage = "Code must be 2 characters", MinimumLength = 2)]
    public string Code { get; set; } = null!;

    [Required]
    [Display(Name = "Name")]
    [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
    public string Name { get; set; } = null!;

    [Required]
    [Display(Name = "Fleet Size")]
    [Range(1, 10000)]
    [RegularExpression("^[0-9]*$", ErrorMessage = "Only numbers are allowed.")]
    public int FleetSize { get; set; } = 1;

    [Required]
    [Display(Name = "Founded Year")]
    [Range(1900, 2100)]
    [RegularExpression("^[0-9]*$", ErrorMessage = "Only numbers are allowed.")]
    public int Founded { get; set; }
}
