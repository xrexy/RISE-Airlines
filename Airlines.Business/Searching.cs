namespace Airlines.Business.Searching;

public enum SearchAlgorithm
{
    Linear = 0,
    Binary = 1
}

public static class Searching
{
    /// <summary>
    /// Searches for <paramref name="target"/> in <paramref name="arr"/> using binary search
    /// </summary>
    /// <returns>The index of <paramref name="target"/> in <paramref name="arr"/> if found, otherwise -1</returns>
    public static int BinarySearch(string[] arr, string target)
    {
        var min = 0;
        var max = arr.Length - 1;
        while (min <= max)
        {
            var mid = min + ((max - min) / 2);
            var comparisonResult = string.Compare(arr[mid], target, StringComparison.OrdinalIgnoreCase);

            if (comparisonResult == 0)
            {
                return mid;
            }
            else if (comparisonResult < 0)
            {
                min = mid + 1;
            }
            else
            {
                max = mid - 1;
            }
        }

        return -1;
    }

    /// <summary>
    /// Searches for <paramref name="target"/> in <paramref name="arr"/> using linear search
    /// </summary>
    /// <returns>The index of <paramref name="target"/> in <paramref name="arr"/> if found, otherwise -1</returns>
    public static int LinearSearch(string[] arr, string target)
    {
        for (var i = 0; i < arr.Length; i++)
        {
            if (string.Equals(arr[i], target, StringComparison.OrdinalIgnoreCase))
            {
                return i;
            }
        }
        return -1;
    }

    /// <summary>
    /// Searches for <paramref name="target"/> in <paramref name="arr"/> using provided <paramref name="algorithm"/>
    /// </summary>
    /// <returns>The index of <paramref name="target"/> in <paramref name="arr"/> if found, otherwise -1</returns>
    /// <exception cref="InvalidOperationException">Thrown if <paramref name="algorithm"/> is not supported</exception>
    public static int Search(SearchAlgorithm algorithm, string[] arr, string target)
    {
        return algorithm switch
        {
            SearchAlgorithm.Linear => LinearSearch(arr, target),
            SearchAlgorithm.Binary => BinarySearch(arr, target),
            _ => throw new InvalidAlgorithmException("Invalid search algorithm")
        };
    }
}
