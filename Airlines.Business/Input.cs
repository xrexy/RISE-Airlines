namespace Airlines.Business.Input;

public enum InputKey
{
    Airline = 1,
    Airport = 2,
    Flight = 3,
    Aircraft = 4
}
