public static class Dijkstra
{
    public static int MiniDist(int[] distance, bool[] tset)
    {
        var minimum = int.MaxValue;
        var index = 0;
        for (var k = 0; k < distance.Length; k++)
        {
            if (!tset[k] && distance[k] <= minimum)
            {
                minimum = distance[k];
                index = k;
            }
        }
        return index;
    }

    public static List<int> Calculate(double?[,] matrix, int src, int dest)
    {
        var length = matrix.GetLength(0);
        var distance = new int[length];
        var explored = new bool[length];
        var prev = new int[length];

        // Initialize
        for (var i = 0; i < length; i++)
        {
            distance[i] = int.MaxValue;
            explored[i] = false;
            prev[i] = -1;
        }
        distance[src] = 0;

        for (var k = 0; k < length - 1; k++)
        {
            // Find the node with minimum distance
            var minNode = MiniDist(distance, explored);
            explored[minNode] = true;

            // Update distance value of the adjacent nodes
            for (var i = 0; i < length; i++)
            {
                if (matrix[minNode, i] > 0)
                {
                    var shortestToMinNode = distance[minNode];
                    var distanceToNextNode = matrix[minNode, i];
                    var totalDistance = shortestToMinNode + distanceToNextNode;

                    // update the distance only if the new distance is less than the current
                    if (totalDistance < distance[i])
                    {
                        distance[i] = (int)totalDistance;
                        prev[i] = minNode;
                    }
                }
            }
        }

        // If destination is not reachable, return empty list
        if (distance[dest] == int.MaxValue)
            return [];

        // Reconstruct path
        var path = new LinkedList<int>();
        var currentNode = dest;
        while (currentNode != -1)
        {
            _ = path.AddFirst(currentNode);
            currentNode = prev[currentNode];
        }
        return [.. path];
    }
}
