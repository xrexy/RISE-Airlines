namespace Airlines.Business.Mapper;

public interface IDtoMapper<TSource, TDestination>
{
  public TDestination Map(TSource source);
  public TSource Map(TDestination destination);
}
