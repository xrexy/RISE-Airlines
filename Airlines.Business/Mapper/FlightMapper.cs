using Airlines.Business.DTO;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Mapper;

public interface IFlightMapper : IDtoMapper<Flight, FlightDTO>;
public class FlightMapper : DtoMapper<Flight, FlightDTO>, IFlightMapper;
