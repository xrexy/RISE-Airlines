public class MenuNavigationFailedException : Exception
{
    public MenuNavigationFailedException() : base("Menu navigation failed. Most likely due to null reference of sub items. Make sure you #InvokeItem before this operation"){ }
    public MenuNavigationFailedException(string? message) : base(message){ }
    public MenuNavigationFailedException(string? message, Exception? innerException) : base(message, innerException){ }
}
