public class InvalidIdentifierException : InvalidOperationException
{
    public InvalidIdentifierException() : base("Invalid Identifier") { }
    public InvalidIdentifierException(string? message) : base(message) { }
    public InvalidIdentifierException(string? message, Exception? innerException) : base(message, innerException) { }
}
