public class InvalidAlgorithmException : InvalidOperationException
{
    public InvalidAlgorithmException() : base("Invalid Algorithm") { }
    public InvalidAlgorithmException(string? message) : base(message) { }
    public InvalidAlgorithmException(string? message, Exception? innerException) : base(message, innerException) { }
}
