#!/bin/bash

for file in $SCRIPTS_DIR/Setup/*.sql; do
    /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "$SA_PASSWORD" -i "$file"
done
