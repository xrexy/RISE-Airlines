document.addEventListener("DOMContentLoaded", () => {
  const btnElement = document.querySelector(".go-top-button");
  if(!btnElement) {
    console.warn("go-top-button requires an element with a .go-top-button class");
    return
  }

  document.addEventListener("scroll", (event) => {
    const maximumHeight = 500;
    const currentHeight = window.scrollY
  
    btnElement.classList.toggle("hidden", currentHeight < maximumHeight);
  })

  btnElement.addEventListener("click", () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  })
})
