document.addEventListener("DOMContentLoaded", () => {
  const tableElement = document.querySelector("table.expandable");
  if (!tableElement) {
    console.error("expandable-table requires a table with class expandable");
    return
  }

  if(!tableElement.querySelector("tbody") || !tableElement.querySelector("thead")) {
    console.error("expandable-table requires a table with a tbody and a thead");
    return
  }

  const size = Number(tableElement.getAttribute("data-max-size")) ?? 3;

  const table = useExpandableTable(tableElement, size);
  table.init();
})

/**
 * Adds a "Show More" button to a table
 * if there are more rows than itemsPerBatch
 * 
 * @param {HTMLTableElement} table
 * @param {number} [itemsPerBatch]
 * 
 */
function useExpandableTable(table, itemsPerBatch) {
  /**
   * @type {HTMLTableElement}
   */
  const tableBody = table.querySelector("tbody");

  let allRows;
  function getAllRows() {
    if (!allRows) allRows = getTableRows();
    return allRows;
  }

  function getTableRows() {
    return Array.from(tableBody.querySelectorAll('tr'));
  }

  function resetTableBody() {
    tableBody.innerHTML = "";
  }

  function createShowMoreButton() {
    const button = document.createElement("button");
    button.id = "form-show-more-btn";
    button.classList.add("btn-primary")
    button.textContent = table.getAttribute("data-expandable-label") || "Show more";

    button.addEventListener("click", () => {
      const allRows = getAllRows();
      const tableRows = getTableRows();

      const currentLength = tableRows.length;

      let desiredLength = currentLength + itemsPerBatch;
      if (desiredLength > allRows.length) {
        desiredLength = allRows.length;
      }

      const toAdd = allRows.slice(currentLength, desiredLength);
      toAdd.forEach(row => tableBody.appendChild(row));

      if (desiredLength === allRows.length) {
        button.remove();
      }
    })

    table.appendChild(button);
  }

  function init() {
    // we initially set allRows to all the tableRows and wipe the whole tableBody
    // so we have a clean "canvas" to work on
    allRows = getAllRows();
    resetTableBody();

    // we then add the first batch of rows
    allRows.slice(0, itemsPerBatch).forEach(row => tableBody.appendChild(row));

    // if there are more rows than we want to show, we add the "Show more" button
    if (allRows.length > itemsPerBatch) {
      createShowMoreButton();
    }
  }

  return {
    getAllRows,
    getTableRows,
    resetTableBody,
    createShowMoreButton,
    init,
  }
}
