using Airlines.Business.DTO;
using Airlines.Business.Mapper;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.Web.Controllers;

public class FlightController(IFlightRepository repository, IFlightMapper mapper) : Controller
{
    [HttpGet]
    public IActionResult Index()
        => this.View(repository.GetAllAsync().Result.ConvertAll(mapper.Map));

    [HttpPost("flight/create")]
    [ValidateAntiForgeryToken]
    public IActionResult Create(FlightDTO dto)
    {
        if (!ModelState.IsValid)
        {
            foreach (var error in ModelState.Values.SelectMany(x => x.Errors))
            {
                Console.WriteLine(error.ErrorMessage);
            }

            return this.RedirectToAction(nameof(Index));
        }

        // TODO airline needs ids, so frontend needs to have dropdowns instead of text inputs
        // var res = repository.AddAsync(new Flight
        // {
        //     AirlineId = model.AirlineId,
        // }).Result;

        // if (res == -1)
        // {
        //     Console.WriteLine("Failed to add flight");
        // }

        return this.RedirectToAction(nameof(Index));
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Search(PropertySearchModel model)
    {
        if (model.SelectedProperty == null || model.Value == null)
        {
            return this.RedirectToAction(nameof(Index));
        }

        // NOTE: this loads ALL airlines in memory which is very bad in a real(any) scenario
        var filtered = repository.GetAllAsync().Result.Where(x => this.MatchesSearchModel(x, model)).ToList();

        // also this will just re-render the view not redirect to it, and if the user reloads the page will be empty
        return this.View("Index", filtered.ConvertAll(mapper.Map));
    }

    // TODO: repeated in multiple controllers, move to a utils file
    private bool MatchesSearchModel(Flight obj, PropertySearchModel model)
    {
        var value = this.GetValueFromPropertyName(obj, model.SelectedProperty);
        return value != null && model.Value.Equals(value, StringComparison.CurrentCultureIgnoreCase);
    }

    private string? GetValueFromPropertyName(object obj, string propertyName)
        => obj.GetType().GetProperty(propertyName)?.GetValue(obj)?.ToString();
}
