public class PropertySearchModel
{
  public string ControllerName { get; set; } = null!;

  public List<string> Properties { get; set; } = [];

  public string SelectedProperty { get; set; } = null!;

  public string Value { get; set; } = null!;
}
