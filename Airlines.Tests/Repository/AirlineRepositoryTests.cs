using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Exceptions;
using Airlines.Persistence.Basic.Repository;
using DbContext = Airlines.Persistence.Basic.Entities.DbContext;

namespace Airlines.Tests.Repository;

public class AirlineRepositoryTests
{
    [Fact]
    public void AirlineRepository_PreprocessEntity_ValidatesCorrectly()
    {
        using var repo = CreateRepository("AirlineRepository_PreprocessEntity_ValidatesCorrectly");

        var currentYear = DateTime.Now.Year;

        void TryPreprocessFail(Airline entity, string failMessage)
        {
            try
            {
                repo!.PreprocessEntity(entity);
            }
            catch (InvalidEntityException e)
            {
                Assert.Equal($"Invalid airline entity; {failMessage}", e.Message);
            }
        }

        TryPreprocessFail(new Airline { FleetSize = 0 }, "FleetSize must be at least 1");
        TryPreprocessFail(new Airline { FleetSize = 1 }, "Name is null");
        TryPreprocessFail(new Airline { FleetSize = 2, Name = string.Join(" ", Enumerable.Range(0, 129)) }, "Name length larger than 128");
        TryPreprocessFail(new Airline { FleetSize = 1, Name = "name", }, "Founded < 1900");
        TryPreprocessFail(new Airline { FleetSize = 1, Name = "name", Founded = 1899 }, "Founded < 1900");
        TryPreprocessFail(new Airline { FleetSize = 1, Name = "name", Founded = currentYear + 10 }, $"Founded > Current Year({currentYear})");
        TryPreprocessFail(new Airline { FleetSize = 1, Name = "name", Founded = currentYear - 2 }, "Code is null");
        TryPreprocessFail(new Airline { FleetSize = 1, Name = "name", Founded = currentYear - 2, Code = "a" }, "Code must be 2 characters");
        TryPreprocessFail(new Airline { FleetSize = 1, Name = "name", Founded = currentYear - 2, Code = "aaa" }, "Code must be 2 characters");
        TryPreprocessFail(new Airline { FleetSize = 1, Name = "name", Founded = currentYear - 2, Code = "aa" }, "No description provided");
        TryPreprocessFail(new Airline { FleetSize = 1, Name = "name", Founded = currentYear - 2, Code = "aa", Description = string.Join(" ", Enumerable.Range(0, 256)) }, "Description length larger than 256");

        var valid = new Airline
        {
            FleetSize = 1,
            Name = "name",
            Founded = currentYear - 2,
            Code = "aa",
            Description = "desc"
        };
        repo.PreprocessEntity(valid);
        Assert.Equal(valid.Code.ToUpper(), valid.Code);
    }

    [Theory]
    [InlineData(5)]
    [InlineData(10)]
    public async void AirlineRepository_GetFunctions(int entitySize)
    {
        using var repo = CreateRepository($"AirlineRepository_GetFunctions_{entitySize}");

        Assert.Empty(await repo.GetAllAsync());
        Assert.Null(await repo.GetByIdAsync(0));

        for (var i = 0; i < entitySize; i++)
        {
            Assert.NotEqual(-1, await repo.AddAsync(new Airline
            {
                Code = "te",
                Founded = DateTime.Now.Year,
                Name = "test",
                Description = "test",
                FleetSize = 1,
                Id = i + 1
            }));
        }

        Assert.Null(await repo.GetByIdAsync(-1));
        Assert.NotNull(await repo.GetByIdAsync(1));

        var allEntities = await repo.GetAllAsync();
        Assert.Equal(entitySize, allEntities.Count);

        for (var i = 0; i < entitySize; i++)
        {
            Assert.Equal(i + 1, allEntities[i].Id);
        }

        var expectedEvenIdsCount = entitySize / 2;
        var entitiesWithEvenIds = await repo.GetAll(x => x.Id % 2 == 0);

        Assert.Equal(expectedEvenIdsCount, entitiesWithEvenIds.Count);

        Assert.NotNull(await repo.GetSingle(x => x.Id == 1));
        Assert.Null(await repo.GetSingle(x => x.Id == -1));
    }

    [Fact]
    public async void AirlineRepository_DeleteFunctions()
    {
        // also tests Exists functions
        using var repo = CreateRepository("AirlineRepository_DeleteFunctions");
        const int size = 5;

        for (var i = 0; i < size; i++)
        {
            Assert.NotEqual(-1, await repo.AddAsync(new Airline
            {
                Code = "te",
                Founded = DateTime.Now.Year,
                Name = "test",
                Description = "test",
                FleetSize = 1,
                Id = i + 1
            }));
        }

        Assert.True(await repo.DeleteAsync(1));
        Assert.False(await repo.ExistsAsync(1));

        var entity = await repo.GetByIdAsync(2);
        Assert.NotNull(entity);
        Assert.True(await repo.ExistsAsync(entity));
        Assert.True(await repo.DeleteAsync(entity));
        Assert.False(await repo.ExistsPredicateAsync(x => x.Id == 2));
    }

    [Fact]
    public async void AirlineRepository_UpdateFunctions()
    {
        using var repo = CreateRepository("AirlineRepository_UpdateFunctions");
        Assert.NotEqual(-1, await repo.AddAsync(new Airline
        {
            Code = "te",
            Founded = DateTime.Now.Year,
            Name = "test",
            Description = "test",
            FleetSize = 1,
            Id = 1
        }));

        var entity = await repo.GetByIdAsync(1);
        Assert.NotNull(entity);

        Assert.False(await repo.UpdateAsync(new Airline { Id = 2 }));
    }

    private static AirlineRepository CreateRepository(string name)
        => new()
        {
            Context = new DbContext(DbContext.ConnectionType.InMemory, name)
        };
}
