
using Airlines.Persistence.Basic.Exceptions;
using Airlines.Persistence.Basic.Repository;
using Microsoft.Data.SqlClient;

namespace Airlines.Tests.Repository;

public class RepositoryHelpersTests
{
    [Fact]
    public void GetSimplifiedExceptionDebugInfo_UnknownError()
    {
        Assert.StartsWith("[Exception] Unknown Error:", GetOutput(new Exception()));
        Assert.StartsWith("[prefix] Unknown Error:", GetOutput(new Exception(), "prefix"));
    }

    [Fact]
    public void GetSimplifiedExceptionDebugInfo_SQLError()
    {
        var exception = MakeSqlException();

        Assert.StartsWith($"[SqlException] SQL Error: {exception.Message}", GetOutput(exception));
        Assert.StartsWith($"[prefix] SQL Error: {exception.Message}", GetOutput(exception, "prefix"));
    }

    [Fact]
    public void GetSimplifiedExceptionDebugInfo_InvalidEntityError()
    {
        var exception = new InvalidEntityException("invalid entity message");

        Assert.StartsWith($"[InvalidEntityException] Invalid Entity: {exception.Message}", GetOutput(exception));
        Assert.StartsWith($"[prefix] Invalid Entity: {exception.Message}", GetOutput(exception, "prefix"));
    }

    private string GetOutput(Exception e, string? prefix = null)
      => RepositoryHelpers.GetSimplifiedExceptionDebugInfo(e, prefix);

    private static SqlException MakeSqlException()
    {
        SqlException? exception = null;
        try
        {
            var conn = new SqlConnection(@"Data Source=.;Database=GUARANTEED_TO_FAIL;Connection Timeout=1");
            conn.Open();
        }
        catch (SqlException ex)
        {
            exception = ex;
        }

        return exception!;
    }
}
