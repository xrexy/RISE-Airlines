using Airlines.Business.Exceptions;
using Airlines.Business.Exceptions.Parser;
using Airlines.Persistence.Basic.Exceptions;

namespace Airlines.Tests;

public class ExceptionsTests
{
    [Fact]
    public void InvalidIdentifierException()
        => ExceptionTestsFactory.CreateBaseExceptionTests<InvalidIdentifierException>("Invalid Identifier");

    [Fact]
    public void InvalidEntityException()
        => ExceptionTestsFactory.CreateBaseExceptionTests<InvalidEntityException>("Invalid Entity");

    [Fact]
    public void InvalidAlgorithmException()
        => ExceptionTestsFactory.CreateBaseExceptionTests<InvalidAlgorithmException>("Invalid Algorithm");

    [Fact]
    public void InvalidInputKeyException()
        => ExceptionTestsFactory.CreateBaseExceptionTests<InvalidInputKeyException>("Invalid InputKey");

    [Fact]
    public void ResourceExistsException()
        => ExceptionTestsFactory.CreateBaseExceptionTests<ResourceExistsException>("Resource already exists");

    [Fact]
    public void MenuInvalidSizeException()
        => ExceptionTestsFactory.CreateBaseExceptionTests<MenuInvalidSizeException>("Invalid menu size");

    [Fact]
    public void MenuNavigationFailedException()
        => ExceptionTestsFactory.CreateBaseExceptionTests<MenuNavigationFailedException>("Menu navigation failed. Most likely due to null reference of sub items. Make sure you #InvokeItem before this operation");

    [Fact]
    public void InvalidPartsSizeException()
        => ExceptionTestsFactory.CreateArgumentExceptionTests<InvalidPartsSizeException>("Received invalid number of parts");

    [Fact]
    public void InvalidParserInputException()
        => ExceptionTestsFactory.CreateArgumentExceptionTests<InvalidParserInputException>("Invalid parser input");
}

internal static class ExceptionTestsFactory
{
    private static void TestException<TException>(string defaultMessage, Func<TException> createException) where TException : Exception
    {
        var exception = createException();
        Assert.Equal(defaultMessage, exception.Message);
    }

    private static void TestException<TException>(string message, Func<string, TException> createException) where TException : Exception
    {
        var exception = createException(message);
        Assert.Equal(message, exception.Message);
    }

    private static void TestException<TException>(Exception innerException, Func<Exception, TException> createException) where TException : Exception
    {
        var exception = createException(innerException);
        Assert.NotNull(exception.InnerException);
        Assert.Equal(innerException, exception.InnerException);
    }

    private static void TestArgumentException<TException>(string paramName, Func<string, string, TException> createException) where TException : ArgumentException
    {
        var exception = createException("msg", paramName);
        Assert.NotNull(exception.ParamName);
        Assert.Equal(paramName, exception.ParamName);
    }

    private static void TestArgumentException<TException>(Exception innerException, Func<Exception, TException> createException) where TException : ArgumentException
    {
        var exception = createException(innerException);

        Assert.NotNull(exception.InnerException);
        Assert.Equal(innerException, exception.InnerException);
    }

    public static void CreateBaseExceptionTests<TException>(string defaultMessage, string customMessage = "Custom Message") where TException : Exception, new()
    {
        // Test default constructor
        TestException(defaultMessage, () => new TException());

        // Test constructor with message
        TestException(customMessage, (msg) => (TException)Activator.CreateInstance(typeof(TException), msg)!);

        // Test constructor with inner exception
        TestException(new Exception(), (ex) => (TException)Activator.CreateInstance(typeof(TException), "", ex)!);
    }

    public static void CreateArgumentExceptionTests<TException>(string defaultMessage, string paramName = "paramName", string customMessage = "Custom Message") where TException : ArgumentException, new()
    {
        CreateBaseExceptionTests<TException>(defaultMessage, customMessage);

        // Test constructor with param name
        TestArgumentException(paramName, (msg, param) => (TException)Activator.CreateInstance(typeof(TException), msg, param)!);

        // Test constructor with inner exception
        TestArgumentException(new Exception(), (ex) => (TException)Activator.CreateInstance(typeof(TException), "", "", ex)!);
    }
}
