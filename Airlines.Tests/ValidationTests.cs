using Airlines.Business.Input;
using Airlines.Business.InputValidation;

namespace Airlines.Tests.ValidationTests;

public class InputValidationTests
{
    [Fact]
    public void ValidateThrows()
        => _ = Assert.Throws<InvalidInputKeyException>(() => InputValidation.Validate((InputKey)9999, "input"));
}

public class AirlineInputValidationTests
{
    [Theory]
    [InlineData("str")]
    [InlineData("good")]
    [InlineData("01234")]
    public void ValidateAirlineValid(string input)
        => Assert.True(InputValidation.Validate(InputKey.Airline, input));

    [Theory]
    [InlineData("toolong")]
    [InlineData("how did I get here")]
    public void ValidateAirlineInvalid(string input)
        => Assert.False(InputValidation.Validate(InputKey.Airline, input));
}

public class AirportInputValidationTests
{
    [Theory]
    [InlineData("AIR")]
    [InlineData("A")]
    public void ValidateAirportValid(string input)
        => Assert.True(InputValidation.Validate(InputKey.Airport, input));

    [Theory]
    [InlineData("thisIsWayyyyTooLong")]
    [InlineData("noSymb@ls")]
    [InlineData("n0Numbers")]
    public void ValidateAirportInvalid(string input)
        => Assert.False(InputValidation.Validate(InputKey.Airport, input));
}

public class FlightInputValidationTests
{
    [Theory]
    [InlineData("Flight")]
    [InlineData("123")]
    [InlineData("l0ngbutf1ne")]
    public void ValidateFlightValid(string input)
        => Assert.True(InputValidation.Validate(InputKey.Flight, input));

    [Theory]
    [InlineData("noSymb@ls")]
    public void ValidateFlightInvalid(string input)
        => Assert.False(InputValidation.Validate(InputKey.Flight, input));

    [Fact]
    public void ValidateAircraft()
        => Assert.True(InputValidation.Validate(InputKey.Aircraft, "aircraft"));
}
