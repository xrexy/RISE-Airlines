using Airlines.Business.Exceptions.Parser;
using Airlines.Persistence.Basic.Models.AircraftTypes;

namespace Airlines.Tests.CMenu;

public class ConsoleMenuExtensionsTests
{
    [Fact]
    public void PopulateInMemoryObjects_PopulateAircraftData_Throws()
    {
        var console = new MockConsole();
        var menu = new ConsoleMenu(console);

        _ = Assert.Throws<InvalidPartsSizeException>(() => ConsoleMenuExtensions.PopulateAircraftData(menu, [["invalid"]]));
    }

    [Fact]
    public void PopulateInMemoryObjects_PopulateAircraftData()
    {
        var console = new MockConsole();
        var menu = new ConsoleMenu(console);

        var lines = GetBaseLines();

        Assert.Equal(0, menu.Aircrafts.Size);
        ConsoleMenuExtensions.PopulateAircraftData(menu, lines);
        Assert.Equal(3, menu.Aircrafts.Size);

        var aircrafts = menu.Aircrafts.GetAll().ToArray();
        Assert.True(aircrafts[0].Type is CargoAircraftType);
        Assert.True(aircrafts[1].Type is PassengerAircraftType);
        Assert.True(aircrafts[2].Type is PrivateAircraftType);
    }

    private static List<string[]> GetBaseLines()
     => [
       ["Boeing 747-8F", "140000", "854.5", "-"],
       ["Airbus A320", "20000", "37.4", "150"],
       ["Gulfstream G650", "-", "-", "18"]
     ];
}
