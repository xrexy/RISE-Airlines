
using Airlines.Business.Input;
using Airlines.Persistence.Basic.Models.AircraftTypes;
using Models = Airlines.Persistence.Basic.Models;

namespace Airlines.Tests.CMenu;

public class ConsoleMenuTests
{
    private static string GetCommandResponse(MockConsole console, ICommand command, List<string> inputs, int messageIdx = 1, bool debug = false)
    {
        inputs.ForEach(console.AddInput);

        console.Clear();
        try
        {
            command.Execute();
        }
        catch (NullReferenceException)
        {
            throw new MenuNavigationFailedException();
        }

        if (debug)
        {
            Console.WriteLine(Helpers.StringifyArray(console.Messages));
        }
        return console.Messages[messageIdx];
    }

    [Fact]
    public void CommandContext_InitializesCorrectly()
    {
#pragma warning disable CS0618
        var ctx = new CommandContext();
#pragma warning restore CS0618

        Assert.NotNull(ctx.AircraftRepository);
        Assert.NotNull(ctx.AirlineRepository);
        Assert.NotNull(ctx.AirportRepository);
        Assert.NotNull(ctx.FlightRepository);
        Assert.NotNull(ctx.RouteRepository);
        Assert.NotNull(ctx.Network);

        Assert.True(ctx.IsIdentifierUnique("test", out var key));
        Assert.Null(key);

        Assert.True(ctx.AirportRepository.Add(new Models.Airport("ap", "name", "city", "country")));
        Assert.True(ctx.AircraftRepository.Add(new Models.Aircraft("pri", new PrivateAircraftType(4))));
        Assert.True(ctx.FlightRepository.Add(new Models.Flight("fl", "ap", "ap2", "pri", 0, 0)));
        Assert.True(ctx.AirlineRepository.Add(new Models.Airline("al", "name")));

        Assert.False(ctx.IsIdentifierUnique("ap", out key));
        Assert.Equal(InputKey.Airport, key);

        Assert.False(ctx.IsIdentifierUnique("fl", out key));
        Assert.Equal(InputKey.Flight, key);

        Assert.False(ctx.IsIdentifierUnique("al", out key));
        Assert.Equal(InputKey.Airline, key);
    }

    [Fact(Timeout = 3000, Skip = "Requires mock DB")]
    public void ConsoleMenu_Command_AddEntry()
    {
        // MockConsole console = new();
        // ConsoleMenu menu = new(console);

        // string TryToAdd(ICommand command, List<string> inputs)
        // {
        //     menu.InvokeItem(1); // navigate to add entry

        //     var res = GetCommandResponse(console, command, inputs);

        //     menu.InvokeItem(0); // go back

        //     return res;
        // }

        // var name = "airline";
        // var identifier = "al";
        // ICommand command = new AddAirlineCommand(menu);
        // Assert.Contains("Added Airline", TryToAdd(command, [name, identifier]));
        // Assert.Contains("is already an Airline", TryToAdd(command, [name, identifier]));

        // identifier = "fl";
        // command = new AddFlightCommand(menu);
        // Assert.Contains("is not a valid aircraft model", TryToAdd(command, [identifier, "dep", "arr", "airc", "1", "1"]));
        // _ = menu.Context.AircraftRepository.Add(new Aircraft("airc", new PrivateAircraftType(10)));

        // Assert.Contains("Added Flight", TryToAdd(command, [identifier, "dep", "arr", "airc", "100", "2,6"]));
        // Assert.Contains("is already an Flight", TryToAdd(command, [identifier, "dep", "arr", "airc", "1", "1"]));

        // name = "airport";
        // identifier = "ap";
        // command = new AddAirportCommand(menu);
        // Assert.Contains("Added Airport", TryToAdd(command, [name, identifier, "city", "country"]));
        // Assert.Contains("is already an Airport", TryToAdd(command, [name, identifier, "city", "country"]));
    }

    [Fact(Timeout = 3000)]
    public void ConsoleMenu_Command_Batching()
    {
        const int batchingId = 4;

        var console = new MockConsole();
        var menu = new ConsoleMenu(console);

        menu.InvokeItem(batchingId);

        var resetBatchingCommand = new ResetBatchingStatusCommand(menu);
        var seeBatchingCommand = new SeeBatchingStatusCommand(menu);
        var toggleBatchingCommand = new ToggleBatchingCommand(menu);

        Assert.Contains("(1) Toggle Batching", GetCommandResponse(console, toggleBatchingCommand, ["n"])); // shouldn't do anything
        Assert.Contains("Batching is DISABLED", GetCommandResponse(console, seeBatchingCommand, []));

        Assert.Contains("Batching Enabled", GetCommandResponse(console, toggleBatchingCommand, ["Y"]));
        Assert.Contains("Batching is ENABLED", GetCommandResponse(console, seeBatchingCommand, []));

        Assert.Contains("(1) Toggle Batching", GetCommandResponse(console, resetBatchingCommand, ["N"]));  // shouldn't do anything
        Assert.Contains("Batching Disabled.", GetCommandResponse(console, toggleBatchingCommand, ["y"]));

        Assert.Contains("Batching is not enabled", GetCommandResponse(console, resetBatchingCommand, ["Y"]));
        Assert.Contains("Batching Enabled", GetCommandResponse(console, toggleBatchingCommand, ["Y"]));
        Assert.Contains("Cleared 0 command(s) and disabled batching", GetCommandResponse(console, resetBatchingCommand, ["Y"]));
    }

    [Fact(Timeout = 3000)]
    public void ConsoleMenu_Command_Routes()
    {
        const int routesIdx = 3;
        const string routeId = "rou";
        // const string flightId = "fl";

        MockConsole console = new();
        ConsoleMenu menu = new(console);

        // menu.PopulateInMemoryObjects("../../../../Airlines.Data/");

        menu.InvokeItem(routesIdx);

        var createNewCommand = new CreateNewRouteCommand(menu);
        var removeLastCommand = new RemoveLastFlightFromRouteCommand(menu);
        // var addFlightCommand = new AddFlightToRouteCommand(menu);
        // var seeFlightsCommand = new SeeRouteFlightsCommand(menu);

        // var shortestRouteCommand = new ShortestRoutePathCommand(menu);
        // var connectivityCheckCommand = new ConnectivityCheckCommand(menu);

        // create new
        Assert.Contains("Added Route", GetCommandResponse(console, createNewCommand, [routeId]));
        Assert.Contains("already exists", GetCommandResponse(console, createNewCommand, [routeId]));

        // remove last flight
        Assert.Contains("does not exist", GetCommandResponse(console, removeLastCommand, ["non"]));
        Assert.Contains("has no flights", GetCommandResponse(console, removeLastCommand, [routeId]));

        // TODO requires mock DB to properly test

        // add flight
        // Assert.False(menu.Context.FlightRepository.Add(new Flight(flightId, "dep", "arr", "dc", 0, 0)));

        // _ = menu.Context.AircraftRepository.Add(new Aircraft("airc", new PrivateAircraftType(10)));
        // _ = menu.Context.FlightRepository.Add(new Flight(flightId, "dep", "arr", "airc", 0, 0));

        // Assert.Contains("does not exist", GetCommandResponse(console, addFlightCommand, ["non"]));
        // Assert.Contains("does not exist", GetCommandResponse(console, addFlightCommand, [routeId, "non"]));
        // Assert.Contains("Added Flight", GetCommandResponse(console, addFlightCommand, [routeId, flightId]));

        // // see routes
        // Assert.Contains("does not exist", GetCommandResponse(console, seeFlightsCommand, ["non"]));

        // console.AddInput(routeId);
        // var seeFlightsRes = seeFlightsCommand.Process();
        // Assert.Equal(0, seeFlightsRes);
        // Assert.Contains("1. FL: DEP -> ARR", console.Messages[1]);

        // // remove last flight
        // Assert.Contains("Removed last flight from Route", GetCommandResponse(console, removeLastCommand, [routeId]));
        // Assert.Contains("has no flights", GetCommandResponse(console, removeLastCommand, [routeId]));

        // Assert.Contains("has no flights", GetCommandResponse(console, seeFlightsCommand, [routeId]));

        // shortest route path
        // Assert.Contains("Invalid Airport Identifier", GetCommandResponse(console, shortestRouteCommand, ["0", "non", "non"]));

        // // TODO implement type
        // Assert.Contains("Shortest path from DFW to SOF is:No Path", GetCommandResponse(console, shortestRouteCommand, ["dfw", "sof"]));
        // Assert.Contains("Shortest path from DFW to SFO is: DFW ->  LAX -> SFO", GetCommandResponse(console, shortestRouteCommand, ["dfw", "sfo"]));

        // // connectivity check command
        // Assert.Contains("does not exist", GetCommandResponse(console, connectivityCheckCommand, ["non"]));
        // Assert.Contains("does not exist", GetCommandResponse(console, connectivityCheckCommand, ["dfw", "non"]));
        // Assert.Contains("DFW is connected to SOF", GetCommandResponse(console, connectivityCheckCommand, ["dfw", "sof"])); // DFW -> ATL -> DCA -> SOF
        // Assert.Contains("DFW is connected to LAX", GetCommandResponse(console, connectivityCheckCommand, ["dfw", "lax"])); // DFW -> LAX
    }

    [Fact(Skip = "Requires mock DB")]
    public void ConsoleMenu_Command_FindAirports()
    {
        // MockConsole console = new();
        // ConsoleMenu menu = new(console);

        // _ = menu.Context.AirportRepository.Add(new Airport("ap", "AP1", "city", "country"));
        // _ = menu.Context.AirportRepository.Add(new Airport("aap", "AP2", "city", "country"));
        // _ = menu.Context.AirportRepository.Add(new Airport("app", "AP_", "city2", "country"));

        // var inCityCommand = new AirportsInCityCommand(menu);

        // console.AddInput("cityy");
        // Assert.Equal(-1, inCityCommand.Proccess());
        // Assert.Contains("No Airports found", console.Messages[1]);

        // console.AddInput("city");
        // Assert.Equal(0, inCityCommand.Proccess());

        // var res = console.Messages[1];
        // Assert.Contains("1. AP1", res);
        // Assert.Contains("2. AP2", res);
        // Assert.DoesNotContain("3. AP_", res);

        // // ---
        // var inCountryCommand = new AirportsInCountryCommand(menu);
        // console.AddInput("countryy");
        // Assert.Equal(-1, inCountryCommand.Proccess());
        // Assert.Contains("No Airports found", console.Messages[1]);

        // console.AddInput("country");
        // Assert.Equal(0, inCountryCommand.Proccess());

        // res = console.Messages[1];
        // Assert.Contains("1. AP1", res);
        // Assert.Contains("2. AP2", res);
        // Assert.Contains("3. AP_", res);
    }

    [Fact(Timeout = 2000, Skip = "Requires mock DB")]
    public void ConsoleMenu_Command_EasyAccess()
    {
        // const string name = "AP1";
        // const string identifier = "AP";

        // var console = new MockConsole();
        // var menu = new ConsoleMenu(console);

        // var command = new EasyAccessAirlineCommand(menu);

        // _ = menu.Context.AirlineRepository.Add(new Airline(identifier, name));

        // console.AddInput(identifier);
        // console.Clear();
        // command.Execute();

        // Assert.Contains($"Valid Airline: {identifier}", console.Messages[1]);

        // console.AddInput("non");
        // console.Clear();
        // command.Execute();

        // Assert.Contains("Airline not found", console.Messages[1]);
    }

    [Fact(Timeout = 3000, Skip = "Requires mock DB")]
    public void ConsoleMenu_Command_Search()
    {
        // var console = new MockConsole();
        // var menu = new ConsoleMenu(console);

        // var command = new SearchCommand(menu);

        // _ = menu.Context.AircraftRepository.Add(new Aircraft("airc", new PrivateAircraftType(10)));
        // _ = menu.Context.AirlineRepository.Add(new Airline("al", "name"));
        // _ = menu.Context.FlightRepository.Add(new Flight("fl", "dep", "arr", "airc", 0, 0));
        // _ = menu.Context.AirportRepository.Add(new Airport("ap", "airport", "city", "country"));

        // // normal finding works
        // console.AddInput("aL");
        // console.Clear();
        // command.Execute();

        // Assert.Contains("is an Airline", console.Messages[1]);

        // // normal finding works
        // console.AddInput("Fl");
        // console.Clear();
        // command.Execute();

        // Assert.Contains("is a Flight", console.Messages[1]);

        // // not found
        // console.AddInput("non");
        // console.Clear();
        // command.Execute();

        // Assert.Contains("is not found", console.Messages[1]);

        // // airport finding works
        // console.AddInput("ap");
        // console.Clear();
        // command.Execute();

        // Assert.Contains("is an Airport", console.Messages[1]);
    }

    // TODO Moved to DatabaseSeeder, not part of ConsoleMenu anymore. Also requires mock db...
    // [Fact]
    // public void ConsoleMenu_Seed()
    // {
    //     var console = new MockConsole();
    //     var menu = new ConsoleMenu(console);
    //     menu.Seed("../../../../Airlines.Data/");

    //     // /home/desktop/Code/rise-airlines/Airlines.Tests/bin/Debug/net8.0/Airlines.Data/airports.csv'.

    //     // -- Airlines
    //     Assert.Equal(5, menu.Context.AirlineRepository.Size);

    //     var airline = menu.Context.AirlineRepository.GetById("AA");
    //     Assert.NotNull(airline);
    //     Assert.Equal("American Airlines", airline.Name);

    //     // -- Airport
    //     Assert.Equal(2, menu.Context.AirportRepository.CountrySize);

    //     var airports = menu.Context.AirportRepository.GetAllByCountry("USA");
    //     Assert.NotNull(airports);
    //     Assert.Equal(11, airports.Count());

    //     // var airport = airports[1];
    //     var airport = airports.First();
    //     Assert.NotNull(airport);
    //     Assert.Equal("JFK", airport.Identifier);
    //     Assert.Equal("USA", airport.Country);
    // }

    [Fact(Skip = "Requires mock db")]
    public void ConsoleMenu_PopulateAirportNetwork()
    {
        // var console = new MockConsole();
        // var menu = new ConsoleMenu(console);

        // var network = menu.Network;

        // _ = ctx.AircraftRepository.Add(new Aircraft("air", new PrivateAircraftType(4)));

        // _ = ctx.AirportRepository.Add(new Airport("id1", "name1", "city1", "country1"));
        // _ = ctx.AirportRepository.Add(new Airport("id2", "name2", "city2", "country2"));
        // _ = ctx.AirportRepository.Add(new Airport("id3", "name3", "city3", "country3"));
        // _ = ctx.AirportRepository.Add(new Airport("id4", "name4", "city4", "country4"));

        // _ = ctx.FlightRepository.Add(new Flight("id1", "id1", "id3", "air", 0, 0));
        // _ = ctx.FlightRepository.Add(new Flight("id2", "id3", "id4", "air", 0, 0));
        // _ = ctx.FlightRepository.Add(new Flight("id3", "id1", "id2", "air", 0, 0));

        // Assert.Empty(network.Nodes);
        // menu.PopulateAirportNetwork();

        // Assert.Equal(4, network.Nodes.Count);

        // Assert.Equal("ID1,ID2,ID3,ID4", Helpers.StringifyArray(network.Labels, ","));
        // var n1 = network.GetNode("id1");
        // Assert.NotNull(n1);
        // Assert.Equal(2, n1.Edges.Count);
    }
}
