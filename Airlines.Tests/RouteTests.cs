using Airlines.Persistence.Basic.Models;

namespace Airlines.Tests;

public class RouteTests
{
    [Fact]
    public void Route_AddFlight()
    {
        var route = new Route("rou");

        route.AddFlight(new Flight("FL", "dep", "arr", "ai", 0, 0));
        _ = Assert.Throws<InvalidOperationException>(() => route.AddFlight(new Flight("FL", "smth", "arr", "ai", 0, 0)));

        route.AddFlight(new Flight("FL2", "arr", "dc", "ai", 0, 0));
    }

    [Fact]
    public void Route_AreLogicallyConnected()
    {
        var route = new Route("rou");
        var f1 = new Flight("FL", "dep", "arr", "ai", 0, 0);
        var f2 = new Flight("FL2", "arr", "dc", "ai", 0, 0);
        Assert.True(route.AreLogicallyConnected(f1, f2));
        Assert.False(route.AreLogicallyConnected(f2, f1));
    }
}
