public class ToggleBatchingCommand(ConsoleMenu menu) : IUnbatchableCommand
{
    ConsoleMenu ICommand.Menu => menu;

    public void Execute()
    {
        var hint = menu.BatchManager.IsBatchingEnabled switch
        {
            true => "You are about to DISABLE batching. This will invoke all batched commands. Are you sure?",
            false => "You are about to ENABLE batching. Are you sure?",
        };

        var response = menu.PromptUntilValid(
            "Toggle? (Y/N): ",
            hint,
            (str) => str.ToUpper() is "Y" or "N"
        ).ToUpper();

        if (response == "Y")
        {
            menu.BatchManager.ToggleBatching();

            menu.PrintItems(menu.CurrentItem!.SubItems, $"Batching {(menu.BatchManager.IsBatchingEnabled ? "Enabled" : "Disabled. All batched commands invoked.")}");
        }
        else
        {
            menu.PrintItems(menu.CurrentItem!.SubItems);
        }
    }
}
