using Airlines.Persistence.Basic.Models;

public class CreateNewRouteCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    public void Execute()
    {
        var identifier = menu.PromptUntilValidCode().ToUpper();

        var route = new Route(identifier);
        var success = menu.Routes.Add(route);

        var message = success ? $"Added Route: {route.Identifier}" : "Route already exists";
        menu.PrintItems(menu.CurrentItem!.SubItems, message);
    }
}
