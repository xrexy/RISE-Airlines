using Airlines.Persistence.Basic.Repository;

public class AirportsInCityCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    public async Task<int> Proccess()
    {
        using var repo = new AirportRepository();
        var city = menu.PromptUntilValid(
            "City: ",
            "Only letters are allowed.",
            (str) => str.All(c => c == ' ' || char.IsLetter(c)) && str.Length is >= 1 and <= 64
        ).ToUpper();

        var airports = await repo.GetAll(x => x.City.Equals(city, StringComparison.CurrentCultureIgnoreCase));
        if (airports.Count == 0)
        {
            menu.GoToInitalPage();
            menu.PrintItems(menu.Items, $"No Airports found in \"{city}\"");
            return -1;
        }

        menu.PrintHeader();
        menu.Console.WriteLine(Helpers.BuildNumberedFormatList(
            airports,
            (airport) => airport.Name
        ));

        return 0;
    }

    public async void Execute()
    {
        if (await this.Proccess() == 0)
        {
            Helpers.BlockUntilKeyPress(menu.Console);
            menu.PrintItems(menu.Items);
        }
    }
}
