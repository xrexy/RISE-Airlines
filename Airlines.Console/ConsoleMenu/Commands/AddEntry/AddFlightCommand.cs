using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Repository;

public class AddFlightCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    public async void Execute()
    {
        using var flightRepo = new FlightRepository();
        using var airportRepo = new AirportRepository();
        using var airlineRepo = new AirlineRepository();

        Task<Airport?> GetAirportWithCode(string code)
        {
            return airportRepo!.GetSingle(x => x.Code.Equals(code, StringComparison.CurrentCultureIgnoreCase));
        }

        var code = menu.PromptUntilValidCode(upperLength: 5, allowDigits: true);

        var fromAirportCode = menu.PromptUntilValid(
            "Departure Airport Code: ",
            "Must be between 2 and 4 characters. Only letters. Single word.",
            (str) => str.All(c => char.IsLetter(c)) && str.Length is >= 2 and <= 4
        );

        var fromAirport = await GetAirportWithCode(fromAirportCode);
        if (fromAirport == null)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"\"Airport {fromAirportCode}\" does not exist");
            return;
        }

        var toAirportCode = menu.PromptUntilValid(
            "Arrival Airport: ",
            "Must be between 2 and 4 characters. Only letters. Single word.",
            (str) => str.All(c => char.IsLetter(c)) && str.Length is >= 2 and <= 4
        );

        var toAirport = await GetAirportWithCode(toAirportCode);
        if (toAirport == null)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"\"Airport {toAirportCode}\" does not exist");
            return;
        }

        var airlineCode = menu.PromptUntilValidCode("Airline ");
        var airline = await airlineRepo.GetSingle(x => x.Code.Equals(airlineCode, StringComparison.CurrentCultureIgnoreCase));
        if (airline == null)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"\"Airline {airlineCode}\" does not exist");
            return;
        }

        var flight = new Flight
        {
            FromAirportId = fromAirport.Id,
            ToAirportId = toAirport.Id,
            AirlineId = airline.Id,
            FlightNumber = code,
            DepartureDateTime = DateTime.Now,
            ArrivalDateTime = DateTime.Now.AddDays(1),
        };
        var addedIdx = await flightRepo.AddAsync(flight);
        var success = addedIdx != -1;

        var message = success ? $"[{addedIdx}] Added Flight: \"{flight.FlightNumber}\"({fromAirport.Name} -> {toAirport.Name})" : "Failed to add flight";
        menu.PrintItems(menu.CurrentItem!.SubItems, message);
    }
}
