public class BatchManager
{
    public Queue<Action> Batches { get; } = [];
    public bool IsBatchingEnabled { get; private set; }

    public void EnableBatching()
    {
        Batches.Clear();
        IsBatchingEnabled = true;
    }

    public void DisableBatching()
    {
        IsBatchingEnabled = false;
        while (Batches.Count > 0)
        {
            Batches.Dequeue()();
        }
    }

    public void ToggleBatching()
    {
        if (IsBatchingEnabled)
        {
            this.DisableBatching();
        }
        else
        {
            this.EnableBatching();
        }
    }

    public void Enqueue(Action action) => Batches.Enqueue(action);
}

public interface IUnbatchableCommand : ICommand;
