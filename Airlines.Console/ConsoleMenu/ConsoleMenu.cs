using Airlines.Business.Structures;
using Airlines.Persistence.Basic.Repository;

public class ConsoleMenu : Menu
{
    public AirportNetwork Network { get; } = new();
    public RouteRepository Routes { get; } = new();
    public AircraftRepository Aircrafts { get; } = new();

    public ConsoleMenu(IConsole console) : base(console)
    {
        this.AddItem(
          "Add Entry",
          [
            new MenuItem("Add Airline", new AddAirlineCommand(this)),
            new MenuItem("Add Flight", new AddFlightCommand(this)),
            new MenuItem("Add Airport", new AddAirportCommand(this)),
          ]
        );

        this.AddItem(
            "Find Airports",
            [
                new MenuItem("In City", new AirportsInCityCommand(this)),
                new MenuItem("In Country", new AirportsInCountryCommand(this)),
            ]
        );

        this.AddItem(
            "Routes",
            [
                new MenuItem("Create New", new CreateNewRouteCommand(this)),
                new MenuItem("Add Flight", new AddFlightToRouteCommand(this)),
                new MenuItem("Remove Last Flight", new RemoveLastFlightFromRouteCommand(this)),
                new MenuItem("See Flights", new SeeRouteFlightsCommand(this)),
                new MenuItem("Connectivity Check", new ConnectivityCheckCommand(this)),
                new MenuItem("Shortest Path", new ShortestRoutePathCommand(this))
            ]
        );

        // this.AddItem(
        //     "Booking",
        //     [
        //         new MenuItem("Cargo Reservation", new CargoReservationCommand(this)),
        //         new MenuItem("Ticket Reservation", new TicketReservationCommand(this))
        //     ]
        // );

        this.AddItem(
            "Batching",
            [
                new MenuItem("Toggle Batching", new ToggleBatchingCommand(this)),
                new MenuItem("Reset Batching Status", new ResetBatchingStatusCommand(this)),
                new MenuItem("See Batching Status", new SeeBatchingStatusCommand(this))
            ]
        );

        this.AddItem("Easy Access(Airline)", new EasyAccessAirlineCommand(this));
    }
}
