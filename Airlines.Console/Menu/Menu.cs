using System.Globalization;

public class Menu(IConsole console)
{
    public IConsole Console { get; } = console;

    public List<MenuItem> Items { get; } = [];
    public MenuItem? CurrentItem { get; protected set; }
    public BatchManager BatchManager { get; } = new();

    public int Size => Items.Count;

    public void AddItem(MenuItem item)
        => Items.Add(item);

    public void AddItem(string label, ICommand command)
        => this.AddItem(new MenuItem(label, command));

    public void AddItem(string label, List<MenuItem> subItems)
        => this.AddItem(new MenuItem(label, subItems));

    public void InvokeItem(int idx)
    {
        var items = CurrentItem?.SubItems ?? Items;
        var isAtRoot = CurrentItem == null;

        if (idx == 0)
        {
            if (isAtRoot)
            {
                Environment.Exit(0);
            }
            else
            {
                CurrentItem = null;
                this.PrintItems(Items);
            }

            return;
        }

        // shift idx -1 since actions are 1-indexed but stored in 0-indexed array
        idx--;

        if (idx < 0 || idx >= items.Count)
        {
            throw new InvalidOperationException("Invalid menu selection.");
        }

        var item = items[idx];
        if (item.Command != null)
        {
            void action()
            {
                this.PrintHeader();
                item.Command.Execute();
            }

            try
            {
                if (BatchManager.IsBatchingEnabled && item.Command is not IUnbatchableCommand)
                {
                    CurrentItem = null;
                    this.PrintItems(Items, $"Batching is enabled. Command will be executed after disabling batching. {BatchManager.Batches.Count}");

                    BatchManager.Enqueue(action);
                }
                else
                {
                    action();
                }
            }
            catch
            {
                this.PrintItems(Items, $"Failed to execute command: {item.Command}");
                throw;
            }
        }
        else
        {
            CurrentItem = item;
            this.PrintItems(item.SubItems);
        }
    }

    public void GoToInitalPage() => CurrentItem = null;

    public string PromptUntilValid(string prompt, string hint, Func<string, bool> isValid)
    {
        while (true)
        {
            this.PrintHeader();

            Console.WriteLine($"[*] {hint}\n");
            Console.Write(prompt);
            var input = Console.ReadLine();
            if (input == null) continue;

            if (isValid(input))
            {
                return input;
            }
        }
    }

    public uint PromptUntilValidUint(string prompt, string hint, int minimum = 1, int maximum = 999)
    {
        var res = this.PromptUntilValid(prompt, hint, (x) =>
        {
            if (!uint.TryParse(x, out var parsed)) return false;
            return parsed >= minimum && parsed <= maximum;
        });

        return uint.Parse(res);
    }

    public double PromptUntilValidDouble(string prompt, string hint, double minimum = 1, double maximum = 999)
    {
        var res = this.PromptUntilValid(prompt, hint, (x) =>
        {
            var valid = double.TryParse(x, NumberStyles.Number, CultureInfo.InvariantCulture, out var parsed);
            return valid && parsed >= minimum && parsed <= maximum;
        });

        return double.Parse(res, NumberStyles.Number, CultureInfo.InvariantCulture);
    }

    public string PromptUntilValidCode(string promptPrefix = "", int lowerLength = 2, int upperLength = 4, bool allowDigits = false)
    {
        var sizeHint = lowerLength == upperLength ? $"{lowerLength}" : $"between {lowerLength} and {upperLength}";
        return this.PromptUntilValid(
            $"{promptPrefix}Identifier: ",
            $"Must be {sizeHint} characters. Only letters. Single word.",
            (str) =>
            {
                var isValidLetters = str.All(c => allowDigits ? char.IsLetterOrDigit(c) : char.IsLetter(c));
                var isProperLength = str.Length >= lowerLength && str.Length <= upperLength;

                return isValidLetters && isProperLength;
            }
        );
    }

    public string PromptUntilValidName(string promptPrefix = "")
    {
        return this.PromptUntilValid(
            $"{promptPrefix}Name: ",
            "Must be at least 4 characters",
            (str) => str.All(c => c == ' ' || char.IsLetterOrDigit(c)) && str.Length is >= 4 and <= 32
        );
    }

    public void PrintHeader(bool shouldClear = true)
    {
        try
        {
            if (shouldClear) Console.Clear();
        }
        catch { }

        Console.WriteLine("--- Airlines Console ---\n");
    }

    // TODO items params is too "low-level" make a function to make usage easier
    public void PrintItems(List<MenuItem> items, string? message = null)
    {
        this.PrintHeader();

        if (message != null)
        {
            Console.WriteLine($"[*] {message}\n");
        }

        for (var i = 0; i < items.Count; i++)
        {
            var item = items[i];
            var hasChildren = item.SubItems.Count > 0;
            var header = hasChildren ? "[{0}]" : "({0})";
            Console.WriteLine($"{string.Format(header, i + 1)} {item.Label}");
        }

        var isAtRoot = CurrentItem != null;
        var footerText = isAtRoot ? "Go Back" : "Exit";
        Console.WriteLine($"\n(0) {footerText}");
    }
}
