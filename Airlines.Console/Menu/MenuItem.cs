public class MenuItem
{
    public string Label { get; }
    public List<MenuItem> SubItems { get; } = [];
    public ICommand? Command { get; }
    public MenuItem? Parent { get; set; }

    public MenuItem(string label, ICommand command)
    {
        Label = label;
        Command = command;
    }

    public MenuItem(string label, List<MenuItem> subItems)
    {
        Label = label;

        foreach (var item in subItems)
        {
            item.Parent = this;
        }
        SubItems = subItems;
    }
}
