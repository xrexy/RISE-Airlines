using System.Text;

public static class Helpers
{
    /// <summary>
    /// Checks if a string is NOT null or empty
    /// </summary>
    /// <returns><c>true</c>, if not empty, <c>false</c> otherwise.</returns>
    public static bool StringNotEmpty(string? str) => !string.IsNullOrEmpty(str);

    /// <summary>
    /// Stringifies an array of strings, skipping empty strings
    /// </summary>
    /// <returns>The stringified array</returns>
    /// <param name="arr">The array to stringify</param>
    public static string StringifyArray<T>(IEnumerable<T> arr, string separator = ", ") => string.Join(separator, arr);

    /// <summary>
    /// !! Blocking !! Reads an integer key from the console (0-9 by default)
    /// </summary>
    /// <returns>The integer key</returns>
    /// <param name="lowerBound">The lower bound of the key (default: '0')</param>
    /// <param name="upperBound">The upper bound of the key (default: '9')</param>
    /// <param name="predefinedKeyChar">(for testing purposes) The predefined key (default: null)</param>
    public static int ReadIntegerKey(char lowerBound = '0', char upperBound = '9', char? predefinedKeyChar = null)
    {
        // do while, because if predefinedKeyChar is provided, needs to run at least once
        // and cannot be just a simple while(true) because if a invalid predefinedKeyChar
        // is provided it will keep looping indefinitely
        do
        {
            var keyChar = predefinedKeyChar ?? Console.ReadKey(intercept: true).KeyChar;
            var isValidKey = keyChar >= lowerBound && keyChar <= upperBound;
            if (isValidKey)
            {
                // transform char to int (-48 is to combat ascii offset)
                return keyChar - 48;
            }
        }
        while (predefinedKeyChar == null);

        return -1;
    }

    /// <summary>
    /// Normalizes a string (ex "Boeing   747-8F" -> "BOEING7478F")
    /// </summary>
    /// <returns>The normalized string</returns>
    /// <param name="str">The string to normalize</param>
    public static string NormalizeString(string str)
        => str.ToUpper().Replace(" ", "");

    /// <summary>
    /// !! Blocking !! Waits for any key to be pressed to continue
    /// </summary>
    public static void BlockUntilKeyPress(IConsole console, string text = "\nPress any key to continue...")
    {
        console.WriteLine(text);
        _ = console.ReadKey();
    }

    /// <summary>
    /// Checks if two strings are equal ignoring case. Basically a wrapper for string.Equals(x, y, StringComparison.OrdinalIgnoreCase);
    /// </summary>
    public static bool StringMatchesIgnoreCase(string? str1, string? str2) => string.Equals(str1, str2, StringComparison.OrdinalIgnoreCase);

    /// <summary>
    /// Builds a numbered format list from an array
    /// </summary>
    /// <returns>The numbered format list</returns>
    /// <param name="arr">The array to format</param>
    /// <param name="formatter">The formatter function(returned value will be displayed after the number)</param>
    public static string BuildNumberedFormatList<T>(IEnumerable<T> arr, Func<T, string> formatter)
    {
        var message = new StringBuilder();

        var count = 1;
        foreach (var item in arr)
        {
            _ = message.AppendLine($"{count++}. {formatter(item)}");
        }

        return message.ToString();
    }
}
